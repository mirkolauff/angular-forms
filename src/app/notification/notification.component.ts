import { Component, OnInit } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  animations: [
    trigger('submitNotificationTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('100ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('100ms', style({ opacity: 0 }))]),
    ]),
  ],
})
export class NotificationComponent implements OnInit {
  isShown = false;

  constructor() {}

  ngOnInit(): void {}

  toggle(value?: boolean) {
    if (value === undefined) this.isShown = !this.isShown;
    else this.isShown = value;
  }
}
